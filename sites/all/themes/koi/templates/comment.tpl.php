<?php
// $Id$
?>
<!-- start comment.tpl.php -->
<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?>">
	<div class="comment-author">
		<?php print $picture ?>
    <cite><?php print $comment->name;?></cite><br>
    <small><strong><?php print format_date($comment->timestamp, 'custom', 'M d Y'); ?></strong> @ <?php print format_date($comment->timestamp, 'custom', 'h:i:s'); ?></small>
  </div>
  <div class="commententry">
   <h3><?php print $title ?></h3>
      <?php print $content ?>
      <?php if ($signature): ?>
      <div class="signature">
        <div>—</div>
        <?php print $signature ?>
      </div>
      <?php endif; ?>
  </div>

  <?php if ($links): ?>
    <div class="reply"><?php print $links ?></div>
  <?php endif; ?>
</div>
<!-- end comment.tpl.php -->
