<?php
// $Id$
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
  <h2 class="node-title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
	<p class="node-date">
  	<span class="day"><?php print $date_day;?></span>
    <span class="month"><?php print $date_month;?></span>
    <span class="year"><?php print $date_year;?></span>
    <span class="nodecomment">
			<?php 
			print $comment_count; 
			if ($comment_count > 1) print t(' Comments');
			else  print t(' Comment');
			?>
    </span>
  </p>
  <p class="node-data">
  	<span class="nodeauthor">by <?php print $name;?></span>
    <?php if ($taxonomy): ?>
    <span class="nodecategory">in <?php print $terms ?></span>
    <?php endif;?>
  </p>

  <div class="content">
    <?php print $content ?>
  </div>

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>

<?php print $node_content_block;?>
</div>
<!-- end node.tpl.php -->