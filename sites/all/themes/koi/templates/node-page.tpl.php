<?php
// $Id$
?>
<div id="node-<?php print $node->nid; ?>">
  <h2 class="node-title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <div class="content">
    <?php print $content ?>
  </div>

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>

<?php print $node_content_block;?>
</div>
<!-- end node.tpl.php -->