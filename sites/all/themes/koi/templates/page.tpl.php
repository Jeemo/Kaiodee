<?php
// $Id$
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<?php print $styles ?>
<?php print $scripts ?>
</head>
<body>
<div id="wrapper">
	<div id="header">
    <h1 id="logo"><a href="<?php print check_url($front_page);?>" title="<?php print $site_name;?>"><?php print $site_name; ?></a></h1>
    <?php if ($site_slogan) : ?>
    <p class="description"><?php print $site_slogan;?></p>
    <?php endif; ?>
    <p class="socialmedia">
    	<?php if ($social_twitter) : ?>
      <a href="http://twitter.com/<?php print $social_twitter;?>"><img src="<?php print $theme_basepath;?>images/socialmedia/twitter.png" alt="Twitter" title="Twitter" />Twitter</a> 
      <?php endif; ?>
      <?php if ($social_facebook) : ?>
      <a href="<?php print $social_facebook;?>"><img src="<?php print $theme_basepath;?>images/socialmedia/facebook.png" alt="Facebook" title="Facebook" />Facebook</a> 
      <?php endif; ?>
      <?php if ($social_flickr) : ?>
      <a href="<?php print $social_flickr;?>"><img src="<?php print $theme_basepath;?>images/socialmedia/flickr.png" alt="Flickr" title="Flickr" />Flickr</a> 
      <?php endif; ?>
      <?php if ($social_rss) : ?>
      <a href="<?php print $social_rss;?>"><img src="<?php print $theme_basepath;?>images/socialmedia/rss.png" alt="RSS" title="RSS" />RSS</a>
      <?php endif; ?>
    </p>

      <?php if (isset($primary_links)) : ?>
        <?php print theme('links', $primary_links, array('id' => 'nav')); ?>
      <?php endif; ?>
      <?php if ($search_box): ?><div id="searchform"><?php print $search_box ?></div><?php endif; ?>
	</div><!-- //header -->
  
  <div id="content">
		<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
    <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
    <?php if ($title): print '<h2'. ($tabs ? ' class="node-title with-tabs"' : ' class="node-title"') .'>'. $title .'</h2>'; endif; ?>
    <?php if ($tabs): print $tabs .'</div>'; endif; ?>
    <?php if ($show_messages && $messages): print $messages; endif; ?>
    <?php print $help; ?>
    <?php print $content ?>
  </div><!-- //Content -->
  <?php if ($right) : ?>
  <div id="sidebar">
  	<?php print $right;?>
  </div><!-- //sidebar -->
	<?php endif; ?>
  <div id="footer">
  	<?php if ($footer1) : ?>
    <div class="footer1">
  		<?php print $footer1;?>
    </div>
    <?php endif; ?>
    <?php if ($footer2) : ?>
    <div class="footer2">
  		<?php print $footer2;?>
    </div>
    <?php endif; ?>
    <?php if ($footer3) : ?>
    <div class="footer3">
  		<?php print $footer3;?>
    </div>
    <?php endif; ?>

    <div class="credits">
      <?php if (isset($credits)) :  print $credits; endif; ?>
      Designed by <a href="http://www.ndesign-studio.com" target="_blank">N.Design</a>. Drupal theme by <a href="http://abthemes.com" target="_blank">Abthemes</a></div>
    </div>

<!--//footer -->

</div>
<!--/wrapper -->
<?php print $closure; ?>
</body>
</html>
