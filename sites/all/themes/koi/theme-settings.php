<?php
// $Id$

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function phptemplate_settings($saved_settings) {
	/**
   * The default values for the theme variables.
   * Matches with $defaults in the template.php file.
   */
	$defaults = array(
    	'google_analytics_id'					=> '',
			'social_twitter'							=> '',
			'social_facebook'							=> '',
			'social_flickr'								=> '',
			'social_rss'									=> '',
	);
	

	
	// Merge the saved variables and their default values
	$settings = array_merge($defaults, $saved_settings);
	// Create theme settings form widgets using Forms API
	// General Settings
	$form['ab_general_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('General settings'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#attributes' => array('class' => 'ab_general_settings'),
	);
		// Google analytics ID
		$form['ab_general_settings']['google_analytics_id'] = array(
		  '#type'          => 'textfield',
		  '#title'         => t('Google analytics ID'),
		  '#default_value' => $settings['google_analytics_id'],
		  '#description'   => t('Insert Google analytics ID'),
		);
	// Social Settings
	$form['ab_social_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('Social settings'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#attributes' => array('class' => 'ab_social_settings'),
	);
		// Twitter
		$form['ab_social_settings']['social_twitter'] = array(
		  '#type'          => 'textfield',
		  '#title'         => t('Twitter username'),
		  '#default_value' => $settings['social_twitter'],
		  '#description'   => t('Insert Twitter username'),
		);
		// Facebook
		$form['ab_social_settings']['social_facebook'] = array(
		  '#type'          => 'textfield',
		  '#title'         => t('Facebook link'),
		  '#default_value' => $settings['social_facebook'],
		  '#description'   => t('Insert Facebook link'),
		);
		// Flickr
		$form['ab_social_settings']['social_flickr'] = array(
		  '#type'          => 'textfield',
		  '#title'         => t('Flickr link'),
		  '#default_value' => $settings['social_flickr'],
		  '#description'   => t('Insert Flickr link'),
		);
		// RSS
		$form['ab_social_settings']['social_rss'] = array(
		  '#type'          => 'textfield',
		  '#title'         => t('RSS link'),
		  '#default_value' => $settings['social_rss'],
		  '#description'   => t('Insert RSS link'),
		);
	// Return theme settings form
	return $form;
}