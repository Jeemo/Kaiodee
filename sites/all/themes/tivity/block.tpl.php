<?php
// $Id: block.tpl.php,v 1.1 2008/10/09 21:01:19 ivansf Exp $
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">
	<?php if (!empty($block->subject)): ?>
  	<h2><?php print $block->subject ?></h2>
	<?php endif;?>
  <div class="content"><?php print $block->content ?></div>
</div>
