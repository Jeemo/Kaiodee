<?php
// $Id: node.tpl.php,v 1.1.2.3 2010/06/21 20:29:18 finex Exp $
?>
<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php if ($page == 0) { ?>
    <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php } ?>
  <div class="content"><?php print $content?></div>
  <?php if ($terms ){ ?>
  <div class="tags"><?php print t("Tags:") . " " . $terms ?></div>
  <?php }?>
  <?php if ($links){ ?>
  <div class="links"><?php print $links ?></div>
  <?php }?>
</div>