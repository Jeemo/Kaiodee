<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?>
<?php
/**
 * Nodewords doesn't correctly print canonical URL, so I print it by myself.
 */
if ($is_front) { ?>
<link rel="canonical" href="<?php print base_path() ?>" />
<?php } else { ?>
<link rel="canonical" href="<?php print url($_GET['q'], array('absolute' => true)); ?>" />
<?php } ?>
<?php print $styles ?>
<?php if ($ie6) { ?>
<!--[if lt IE 7]>
<style type="text/css" media="all"><?php print $ie6_style_hack ?></style>
<![endif]-->
<?php } ?>
<?php print $scripts ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body>
<div class="fixed"> <!--FIXED-->
  <?php if($persistent_mission) { ?><div id="mission"> <!--MISSION-->
    <?php print $persistent_mission ?>
  </div> <!--END MISSION--><?php } ?>
  <div id="top"> <!--TOP-->
    <?php if($logo) { ?><span id="site-logo"><a href="<?php print $front_page ?>" title="<?php print $site_name ?>"><img src="<?php print $logo ?>" alt="<?php print $site_name ?>" /></a></span><?php } ?>
    <?php if($site_name || $site_slogan) {?><div id="site-info"> <!--SITE INFO-->
    <?php if($site_name) { ?><span id='site-name'><a href="<?php print $front_page ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a></span><?php } ?>
    <?php if($site_slogan) { ?><span id='site-slogan'><?php print $site_slogan ?></span><?php } ?>
    </div> <!--END SITE INFO--><?php } ?>
    <?php if(isset($secondary_links)){?><div id="secondary"> <!--SECONDARY LINKS-->
      <?php print theme('links', $secondary_links) ?>
    </div> <!--END SECONDARY LINKS--><?php } ?>
  </div> <!--END TOP-->
  <div id="transparent_top">&nbsp;</div>
  <div id="transparent_menu"> <!--TRANSPARENT MENU-->
    <div id="menu"> <!--MENU-->
      <div id="primary"> <!--PRIMARY-->
        <?php print theme('links', $primary_links) ?>
      </div> <!--END PRIMARY-->
      <div id="search_box"> <!--SEARCH-->
        <?php print $search_box ?>
      </div> <!--END SEARCH-->
    </div> <!--END MENU-->
  </div> <!--END TRANSPARENT MENU-->
  <div id="transparent"> <!--TRANSPARENT-->
    <div id="wrapper"> <!--WRAPPER-->
      <div id="container"> <!--CONTAINER-->
        <?php if ($content_top){ ?><div id="content_top"><!--CONTENT_TOP-->
          <?php print $content_top; ?>
        </div><!--END CONTENT_TOP--><?php } ?>
        <div id="content"> <!--CONTENT-->
          <?php if ($title){?><?php if($is_front){ ?><h1 id="title"><?php print $title ?></h1><?php } else { ?><h1 id="title"><a href="<?php print url($_GET['q'], array('absolute' => true)); ?>" title="<?php print $title ?>"><?php print $title ?></a></h1><?php } ?><?php } ?>
          <div id="main"> <!--MAIN-->
            <?php if($tabs){?><div class="tabs">
              <?php print $tabs ?>
            </div><?php } ?>
            <?php if ($show_messages) { print $messages; } ?>
            <?php print $help ?>
            <?php print $content; ?>
          </div> <!--END MAIN-->
        </div> <!--END CONTENT-->
        <?php if ($content_bottom){ ?><div id="content_bottom"><!--CONTENT_BOTTOM-->
          <?php print $content_bottom; ?>
        </div><!--END CONTENT_BOTTOM--><?php } ?>
      </div> <!--END CONTAINER-->
      <div id="sidebar"> <!--SIDEBAR-->
        <?php if ($sidebar) { print $sidebar; } ?>
      </div> <!--END SIDEBAR-->
    </div> <!--END WRAPPER-->
  </div> <!--END TRANSPARENT-->
  <div id="transparent_wrapper_bottom">
    <div id="wrapper_bottom">&nbsp;</div>
  </div>
  <?php if($footer_message){?><div id="footer_message"> <!--FOOTER MESSAGE-->
    <?php print $footer_message ?>
  </div> <!--END FOOTER MESSAGE--> <?php } ?>
  <div id="transparent_bottom">&nbsp;</div>
  <div id="credits"><?php print credits(); ?></div>
</div> <!--END FIXED-->
<?php print $closure ?>
</body>
</html>