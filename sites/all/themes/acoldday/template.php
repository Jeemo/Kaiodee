<?php
// $Id: template.php,v 1.1.2.2 2010/02/12 10:04:59 finex Exp $

function phptemplate_stylesheet_import($stylesheet, $media = 'all') {
  if (strpos($stylesheet, 'misc/drupal.css') == 0) {
    return theme_stylesheet_import($stylesheet, $media);
  }
}

function acoldday_preprocess_page(&$vars) {
  $vars['persistent_mission'] = variable_get('site_mission', '');
  $vars['ie6'] = FALSE;
  $vars['ie6_style_hack'] = acoldday_ie6_style_hack();
  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') !== false)){ $vars['ie6'] = TRUE; }
}

function acoldday_filter_tips_more_info() { return ''; }

function credits(){
  return "<h5>by <a href=\"http://www.realizzazione-siti-vicenza.com\" title=\"Drupal development, E-commerce\">R</a> &amp; <a href=\"http://www.themes-drupal.org\" title=\"Drupal template design\">T</a></h5>";
}

function acoldday_ie6_style_hack() {
  $elements = array(
    "transparent",
    "wrapper",
    "transparent_top",
    "transparent_bottom",
    "transparent_wrapper_bottom",
    "transparent_menu",
    "wrapper_bottom",
    "footer_message",
    "top",
    "search_box"
  );
  $style_transparency = " ";
  $style_links = " ";
  foreach ($elements as $e){
    $style_transparency = $style_transparency . " #" . $e . " { background:transparent; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale, src=" . base_path() . path_to_theme() . "/img/" . $e . ".png); } ";
    $style_links = $style_links . " #" . $e . " a, input { position:relative }";
  };
  return $style_transparency . $style_links . " #wrapper,#transparent{width:940px}";
}

?>