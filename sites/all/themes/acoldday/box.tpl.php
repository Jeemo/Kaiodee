<?php
// $Id: box.tpl.php,v 1.1.2.2 2010/02/12 10:04:59 finex Exp $
?>
<div class="box">
  <?php if ($title) { ?><h2 class="title"><?php print $title; ?></h2><?php } ?>
  <div class="content"><?php print $content; ?></div>
</div>