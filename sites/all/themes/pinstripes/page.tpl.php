<?php
// $Id$
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?>
<?php print $styles; ?>
<?php print $scripts; ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>

<body class="<?php print $body_classes; ?>">

<!-- begin page section -->
  <div id="page">

<!-- begin header section -->
    <div id="header">

<!-- begin logo-title section -->
      <div id="logo-title">

        <?php if (!empty($logo)): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>

<!-- begin name-and-slogan section -->
        <div id="name-and-slogan">
          <?php if (!empty($site_name)): ?>
            <h1 id="site-name">
              <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>

<!-- begin site-slogan section -->
          <?php if (!empty($site_slogan)): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
        </div> <!-- end of name-and-slogan section -->
      </div> <!-- end of logo-title section -->

      <?php if (!empty($header)): ?>
        <div id="header-region">
          <?php print $header; ?>
        </div>
      <?php endif; ?>

    </div> 
<!-- end header section -->

<!-- begin page content container -->
    <div id="container" class="clear-block">

<!-- the Primary Links and breadcrumbs go right under the header, before the three column content areas -->
      <div id="navigation" class="menu <?php if (!empty($primary_links)) { print "withprimary"; } ?> ">
        <?php if (!empty($primary_links)): ?>
          <div id="primary" class="clear-block">
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
          </div> <!-- end Primary Links -->
        <?php endif; ?>
        <?php if (!empty($breadcrumb)): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
      </div> <!-- end breadcrumbs -->

<!-- begin the left sidebar -->
      <?php if (!empty($left)): ?>
        <div id="sidebar-left" class="column sidebar">
          <?php print $left; ?>
        </div> <!-- end sidebar-left -->
      <?php endif; ?>

<!-- begin the main content column -->
      <div id="main" class="column"><div id="main-squeeze">

<!-- the mission statement -->
        <?php if (!empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>

<!-- main page content: title, tabs, messages, help, content, and feed icons -->
        <div id="content">
          <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
          <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
          <?php if (!empty($help)): print $help; endif; ?>
          <div id="content-content" class="clear-block">
            <?php print $content; ?>
          </div> <!-- end of main content (content-content) -->
          <?php print $feed_icons; ?>
        </div> <!-- end of main page content -->

      </div></div> <!-- end of the main content column -->

<!-- begin the right sidebar -->
      <?php if (!empty($right)): ?>
        <div id="sidebar-right" class="column sidebar">
          <?php print $right; ?>
        </div> <!-- end of sidebar-right -->
      <?php endif; ?>

    </div> <!-- end of the page content container -->

<!-- begin the footer: optional secondary links and footer message -->
    <div id="footer-wrapper">
      <div id="footer">
        <?php if (!empty($secondary_links)): ?>
          <div id="secondary" class="clear-block">
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
          </div>
        <?php endif; ?>
        <?php print $footer_message; ?>
        <?php if (!empty($footer)): print $footer; endif; ?>
      </div> <!-- end footer -->
    </div> <!-- end footer-wrapper -->
<!-- end of footer -->

    <?php print $closure; ?>

  </div> 
<!-- end of page -->

</body>
</html>
