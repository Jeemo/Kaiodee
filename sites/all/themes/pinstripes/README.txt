$Id

Some Tips:

The pinstripes theme has three style sheets enabled as part of the theme package. In addition to the "style.css" document that controls the display of the content on a computer screen or while using a projector, a second style sheet controls how the page content is printed. The third style sheet has been enabled for use by site visitors with cell phones, pocket PCs, smart phones, and other mobile devices. These style sheets will be called into use automatically, depending on the type of device that is retrieving a particular page from your site. There's no need to use a "switch theme" module or scripting.

These style sheets also contain built-in style classes that you can use to control what page content will be printed or displayed with each media type, and you can include these classes while coding your content.

For example, you should suppress the display of large images on mobile devices. While these devices can reduce an image as needed, the actual file size of the image isn't reduced. A 300-meg image will remain a 300-meg image, even if it's being displayed on a cell phone screen that is only 150 pixels wide. Also, you need to take bandwidth issues into account: some of your users may be visiting your site using a relatively slow cell phone network.

To hide a large image, I've created a special "dontShow" style class and it's included in the "handheld.css" style sheet. All you need to do is to add a "class="dontShow" attribute and value pair to the XHTML element code for the image. For example:
<img src=largeimage.png" class="dontShow" height="450" width="750" alt="view of the City of San Francisco, California" />

In the Pinstripes theme, the printer friendly style sheet ("print.css") has been set up so that nothing in the left or right sidebars will be printed. Also, the Primary Links menu won't be printed (if you're using the default placement, which is right under the page header).

You may want to suppress the printing of a particular chunk of content within the main page content column as well. For example, if you have a custom search form on one of your pages, there's a "dontPrint" style class that you can use to suppress the printing of this search feature. This style class can be used with a variety of XHTML elements, including <p>, <span>, and <div>. For example:
<div class="dontPrint">
... custom search form code ...
</div>

For more information about mobile friendly style sheets, see my guide (PDF):
http://www.lib.berkeley.edu/digicoll/libraryweb/mobile-websites.pdf

Thanks for your interest in the pinstripes theme!

Fleur Helsingor
fleur@well.com

