/**
* 2010/08/15 08:20:32 BigCat Software
* round corners for IE <9
**/

DD_roundies.addRule('#columnB_3columns .block', '7px');
DD_roundies.addRule('#featured .block', '7px');
DD_roundies.addRule('#main_inner .comment', '7px');
DD_roundies.addRule('.form-submit', '5px');
DD_roundies.addRule('#columnB_3columns h2', '5px');
