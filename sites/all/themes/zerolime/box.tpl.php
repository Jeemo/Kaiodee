<?php
// $Id: box.tpl.php,v 1.1 2010/10/05 12:24:27 trisz Exp $
// Drupal themes designed by Nemketto.com.
// Created Aug 15, 2010, Last Updated: Aug 15, 2010
?>
<div class="box">
  <?php if ($title) { ?><h2 class="title"><?php print $title; ?></h2><?php } ?>
  <div class="content"><?php print $content; ?></div>
</div>

