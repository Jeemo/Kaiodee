<?php
// $Id: page.tpl.php,v 1.1.2.2 2010/10/30 18:02:01 trisz Exp $
// Drupal themes designed by Nemketto.com.
// Created Aug 15, 2010, Last Updated: Aug 15, 2010
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>

  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?>
  </script>

  <link rel="stylesheet" type="text/css" title="Red" href="style.css" />


  <!--[if IE 6]>
    <link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme(); ?>/style.ie6.css" />
  <![endif]-->

  <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme(); ?>/style.ie7.css" />
  <![endif]-->

  <!--[if IE]>
    <script type="text/javascript" src="<?php print $base_path . $directory; ?>/DD_roundies_0.0.2a.js"></script>
    <script type="text/javascript" src="<?php print $base_path . $directory; ?>/ie.js"></script>
  <![endif]-->

  
</head>
<body>
  <div id="header">
    <div id="header_inner">
      <div id="logo">
        <?php if ($logo) { ?><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a><?php } ?>
        <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></h1><?php } ?>
        <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan; ?></div><?php } ?>
      </div>
      <div id="menu">
        <?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' => 'links', 'id' => 'subnavlist')); ?><?php } ?>
        <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')); ?><?php } ?>
      </div>
      <div class="navbg"></div>
    </div>
  </div>

  <div id="main">
    <div id="secondaryContent_3columns">
      <div id="columnB_3columns">
        <?php print $search_box; ?>
        <?php print $left; ?>
      </div>
    </div>
    <div id="main_inner" class="fluid">
      <div id="primaryContent_3columns">
        <?php if ($featured): ?>
        <div id="featured_3columns">
          <div id="featured" class="featured">
            <?php echo $featured; ?>
          </div>
        </div>
        <?php endif; ?>
        <div id="columnA_3columns">
          <?php if ($mission) { ?><div id="mission"><?php print $mission; ?></div><?php } ?>
          <?php print $breadcrumb; ?>
          <h1 class="title"><?php print $title; ?></h1>
          <div class="tabs"><?php print $tabs; ?></div>
          <?php if ($show_messages) { print $messages; } ?>
          <?php print $help; ?>
          <?php print $content; ?>
          <?php print $feed_icons; ?>
          <br class="clear" />
          <div class="post"></div>
        </div>
      </div>
      <br class="clear" />
    </div>
  </div>

  <br class="clear" />
  <br class="clear" />

  <div class="Footers">
    <div class="Footer3">
      <div class="footer_left">
        <?php echo $footer_left; ?>
      </div>
      <div class="footer_middle">
        <?php echo $footer_middle; ?>
      </div>
      <div class="footer_right">
        <?php echo $footer_right; ?>
      </div>
    </div>
    <br class="clear" />
    <div class="Footer">
      <div class="footer_bottom">
        <?php echo $footer_bottom; ?>
      </div>
    </div>
  </div>

  <div class="page-footer"><?php echo $footer_message; ?></div>

  <?php print $closure; ?>
</body>
</html>