<?php
// $Id: comment.tpl.php,v 1.1 2010/10/05 12:24:27 trisz Exp $
// Drupal themes designed by Nemketto.com.
// Created Aug 15, 2010, Last Updated: Aug 15, 2010
?>
<div class="comment<?php print ' '. $status; ?>">
  <?php if ($picture) {
  print $picture;
  } ?>

  <div class="commentbubble">
    <h3 class="title"><?php print $title; ?></h3><?php if ($new != '') { ?><span class="new"><?php print $new; ?></span><?php } ?>
      <div class="content">
        <?php print $content; ?>
        <?php if ($signature): ?>
        <div class="clear-block">
          <div>—</div>
          <?php print $signature; ?>
        </div>
        <?php endif; ?>
      </div>
    <div class="links">&raquo; <?php print $links; ?></div>
  </div>

  <div class="submitted"><?php print $submitted; ?></div>

</div>
