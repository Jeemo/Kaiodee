/* $Id*/

-- SUMMARY --

This text will follow soon...

-- REQUIREMENTS --

* Drupal 6 and Storm.

* Storm_QuickTT (optional)

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

No configuration available or required at this point. Just go to
http://www.example.com/stormdashboard to start dashboarding dynamically.

-- CUSTOMIZATION --

None

-- TROUBLESHOOTING --

Not known yet

-- FAQ --

Yet to come

-- CONTACT --

Current maintainer:
* J�rgen Haas (jurgenhaas) - http://drupal.org/user/168924

This project has been sponsored by:
* PARAGON Executive Services GmbH
  Providing IT services as individual as the requirements. Find out more
  from http://www.paragon-es.de

